console.log('Hello David');

// JSON - Javascript Object Notation
// a popular data format for application to communicate with each other.
// It may look like a javascript object, but it is actually a string.

let sample1 = `{
	"name": "Katniss Everdeen",
	"age": 20,
	"address": {
		"city": "Kansas City",
		"state": "Kansas"
	}
}
`;

console.log(sample1);

// JSON.Parse() - will return the JSON as JS Objects

console.log(JSON.parse(sample1));

// JSON Array is an array of JSON

let sampleArr = `
	[
		{
			"email": "jasonNewsted@gmail.com",
			"password": "iplaybass1234",
			"isAdmin": false
		},
		{
			"email": "larsDrums@gmail.com",
			"password": "metallicaMe80",
			"isAdmin": true
		}
	]
`;

console.log(sampleArr);

let parsedSampleArr = JSON.parse(sampleArr);
console.log(parsedSampleArr);

console.log(parsedSampleArr.pop());
console.log(parsedSampleArr);

sampleArr = JSON.stringify(parsedSampleArr);
console.log(sampleArr);

let jsonArr = `	[
		"pizza",
		"humburger",
		"spaghetti",
		"shanghai",
		"hotdog stick on a pineapple",
		"pancit bihon"
	]
`
let parsedJsonArr = JSON.parse(jsonArr);
parsedJsonArr.pop();
parsedJsonArr.push("fries");
console.log(parsedJsonArr);

let stringJsonArr = JSON.stringify(parsedJsonArr);
console.log(stringJsonArr);